from django.urls import include,path
from .views import index, about, aktivitassaya

urlpatterns = [
    path('', index, name='index'),
    path('about/', about, name='about'),
    path('aktivitassaya/', aktivitassaya, name='aktivitassaya'),
]
